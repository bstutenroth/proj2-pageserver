from flask import Flask
from flask import send_from_directory
from flask import request
from flask import render_template


app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:path>", methods=['GET'])
def trivia(path):
    if request.method == 'GET' and (str(path) =="trivia.html" or str(path) =="trivia.css"):
        return send_from_directory('', path)
    elif request.method == 'GET' and ("~" in str(path) or "//" in str(path) or ".." in str(path)):
        return send_from_directory('', "403.html")
    elif request.method == 'GET' and (str(path) !="trivia.html" or str(path) !="trivia.css"):
        return send_from_directory('', "404.html")
        

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
